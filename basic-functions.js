/* Task 1*/
function max(i, j) {
	if (isNaN(i)) {
		throw "parameter 'i' with value = '" + i + "' is not a number.";
	}
	if (isNaN(j)) {
		throw "parameter 'j' with value = '" + j + "' is not a number.";
	}
	if (i > j) {
		return i;
	}
	else if (i < j) {
		return j;
	}
	else { //equal
		return i; //or j, doesn't matter because they are equal.
	}
}

/* Task 2 */
function maxOfThree(i, j, k) {
	if (isNaN(i)) {
		throw "parameter 'i' with value = '" + i + "' is not a number.";
	}
	if (isNaN(j)) {
		throw "parameter 'j' with value = '" + j + "' is not a number.";
	}
	if (isNaN(k)) {
		throw "parameter 'k' with value = '" + k + "' is not a number.";
	}
	//todo - do what you need
}
function maxOfThree_v2() {
	for (var i = 0; i < arguments.length; i++) {
		if (isNaN(arguments[i])) {
			throw "parameter #" + (i + 1) + " with value = '" + arguments[i] + "' is not a number.";
		}
	}
}

/* Task 3 */
function isVowel(c) {
	var vowels = ['a', 'e', 'i', 'o', 'u'];
	return vowels.indexOf(c.toLowerCase()) !== -1
}

/* Task 4 */
function translate(sentences) {
	var consonants = ['q', 'w', 'r', 't', 'y', 'p', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'z', 'x', 'c', 'v', 'b', 'n', 'm'];
	var output = "";
	var additionalSymbol = 'o';
	for (var i = 0; i < sentences.length; i++) {
		var c = sentences[i];
		if (consonants.indexOf(c.toLowerCase()) !== -1) {
			output += c + additionalSymbol + c;
		}
		else {
			output += c;
		}
	}
	return output;
}

/* Task 5 */
function sum(array) {
	var sum = 0;
	for (var i = 0; i < array.length; i++) {
		var item = array[i];
		if (isNaN(item)) {
			throw 'Element in position #' + (i + 1) + ' is not a number.';
		}
		sum += Number(item);
	}
	return sum;
}

function multiply(array) {
	var multiply = 1;
	for (var i = 0; i < array.length; i++) {
		var item = array[i];
		if (isNaN(item)) {
			throw 'Element in position #' + (i + 1) + ' is not a number.';
		}
		multiply *= Number(item);
	}
	return multiply;
}

/* Task 6 */
function reverse(text) {
	var output = "";
	for (var i = text.length - 1; i >= 0; i--) {
		output += text[i];
	}
	return output;
}

/* Task 7*/
function translateChristmasCard(text, dictionary) {
	if (dictionary != undefined && typeof dictionary != "object") {
		throw "Input parameter is not an key-value object.";
	}
	var dict = dictionary;
	if (dict == undefined) {
		dict = {"merry":"god", "christmas":"jul", "and":"och", "happy":"gott", "new":"nytt", "year":"år"};
	}
	var outputArr = [];
	var words = text.split(" ");
	for (var i = 0; i < words.length; i++) {
		var word = words[i];
		var val = dict[word];
		if (val != undefined) {
			outputArr.push(val);
		}
		else {
			outputArr.push(word);
			console.error("Not translation for word '" + word + "'.");
		}
	}
	return outputArr.join(" ");
}

/* Task 8 */
function findLongestWord(wordsArray) {
	var longestLength = 0;
	for (var i = 0; i < wordsArray.length; i++) {
		var wordLength = wordsArray[i].length;
		if (wordLength > longestLength) {
			longestLength = wordLength;
		}
	}
	return longestLength;
}

/* Task 9 */
function filterLongWords(wordsArray, biggerThan) {
	if (isNaN(biggerThan)) {
		throw "biggerThan is not a number.";
	}
	var outputArr = [];
	for (var i = 0; i < wordsArray.length; i++) {
		var word = wordsArray[i];
		if (word.length > biggerThan) {
			outputArr.push(word);
		}
	}
	return outputArr;
}

/* Task 10 */
function charFreq(input) {
	var outputData = {};
	for (var i = 0; i < input.length; i++) {
		var c = input[i];
		if (outputData[c] == undefined) {
			outputData[c] = 1;
		}
		else {
			outputData[c]++;
		}
	}
	return outputData;
}

/* Task 11 */
function checkValue(amount) {
	if (isNaN(amount)) {
		throw "amount is not a number.";
	}
	var output = amount;
	if (amount == 1000000) {
		output += " dollars ($$$)"
	}
	else {
		output += " leva";
	}
	return output;
}

/* Task 12*/
function mixUp (firstWord, secondWord) {
	if (firstWord == undefined) {
		throw "firstWord is undefined";
	}
	if (secondWord == undefined) {
		throw "secondWord is undefined";
	}
	var symbolsStep = 2;
	var secondWordSymbols = secondWord.substr(0, symbolsStep);
	var firstWordSymbols = firstWord.substr(0, symbolsStep);
	return secondWordSymbols + firstWord.substr(symbolsStep) + " " + firstWordSymbols + secondWord.substr(symbolsStep);
}

/* Task 13 */
function fixStart(input) {
	if (input == null || input == undefined) {
		throw "Input string can't be null or undefined!";
	}
	if (input.trim() == "") {
		throw "Input string can't be empty.";
	}
	var encode = "*";
	var firstChar = input[0];
	var output = firstChar;
	for (var i = 1; i < input.length; i++) { //start from 1 because don't need to check the first symbol.
		var c = input[i];
		if (firstChar.toLowerCase() == c.toLowerCase()) {
			c = encode;
		}
		output += c;
	}
	return output;
}

/* Task 14 */
function verbing(word) {
	var suffixEnd = "ing";
	var suffinLy = "ly";
	if (word.length < 3) {
		return word;
	}
	else {
		if (word.endsWith(suffixEnd)) {
			return word + suffinLy;
		}
		else {
			return word + suffixEnd;
		}
	}
}

/* Task 15*/
function notBad(sentence) {
	var leftPartIndex = null;
	var rightPartIndex = null;
	var leftPart = "not";
	var rightPart = "bad";
	var replaceWith = "good";
	
	var words = sentence.split(" ");
	var wordsCopy = words.slice();
	
	while (bothPartsExist(wordsCopy)) {
		wordsCopy = replaceNotBadInArray(wordsCopy);
	}
	
	return wordsCopy.join(" ");
	
	function bothPartsExist(arr) {
		var tempSep = "##||##";
		var arrLowerCase = arr.join(tempSep).toLowerCase().split(tempSep);
		return arrLowerCase.indexOf(leftPart) > -1 && arrLowerCase.indexOf(rightPart) > -1;
	}
	
	function replaceNotBadInArray(currentArr) {
		var currentArrCopy = currentArr.slice();
		for (var i = 0; i < currentArr.length; i++) {
			var word = currentArr[i];
			if (word.toLowerCase() == leftPart) {
				leftPartIndex = i;
			}
			if (word.toLowerCase() == rightPart) {
				rightPartIndex = i;
			}
			if (leftPartIndex != null && rightPartIndex != null) {
				currentArrCopy.splice(leftPartIndex, (rightPartIndex - leftPartIndex + 1), replaceWith);
				leftPartIndex = null;
				rightPartIndex = null;
				break;
			}
		}
		return currentArrCopy;
	}
}

/* Task 16 */
function removeRandomItem(arr) {
	if (arr.length == 0) {
		return null;
	}
	var itemToRemove = Math.floor(Math.random() * arr.length - 1);
	var removed = arr.splice(itemToRemove, 1);
	return removed[0];
}

/* Task 17 */
function randomizeOrder(arr) {
	if (arr.length == 0) {
		return [];
	}
	var output = [];
	
	while (arr.length > 0) {
		output.push(removeRandomItem(arr));
	}
	
	return output;
}
